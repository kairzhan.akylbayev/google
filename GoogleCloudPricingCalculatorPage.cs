﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Google
{
    public class GoogleCloudPricingCalculatorPage
    {
        private readonly IWebDriver driver;
        private const string CalculatorUrl = "https://cloud.google.com/products/calculator";
        private IWebElement secondLink;

        public GoogleCloudPricingCalculatorPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void ClickSecondLink()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            secondLink = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector($"a[href='https://cloud.google.com/products/calculator-legacy?hl=es-419']")));

            secondLink.Click();
        }


        public void SwitchToCalculatorFrame()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            var calculatorFrame = wait.Until(ExpectedConditions.ElementIsVisible(By.TagName("iframe")));

            driver.SwitchTo().Frame(calculatorFrame);

        }


        public void FillOutForm()
        {
            SwitchToCalculatorFrame();

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            var inputField = wait.Until(ExpectedConditions.ElementIsVisible(By.Name("quantity")));

            inputField.Clear();

            inputField.SendKeys("4");

            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("document.getElementById('select_113').click();");
            String findAndClickOptionScript00 =
              "var options = document.querySelectorAll('option, md-option, div[role=\"option\"]');" + // Adjust the selector based on your actual options' elements
              "for(var i = 0; i < options.length; i++) {" +
              "  if(options[i].textContent.trim() === 'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)') {" +
              "    options[i].click();" +
              "    break;" +
              "  }" +
              "}";
            js.ExecuteScript(findAndClickOptionScript00);

            IJavaScriptExecutor js1 = (IJavaScriptExecutor)driver;
            js1.ExecuteScript("document.getElementById('select_value_label_93').click();");
            String findAndClickOptionScript01 =
              "var options = document.querySelectorAll('option, md-option, div[role=\"option\"]');" + // Adjust the selector based on your actual options' elements
              "for(var i = 0; i < options.length; i++) {" +
              "  if(options[i].textContent.trim() === 'Regular') {" +
              "    options[i].click();" +
              "    break;" +
              "  }" +
              "}";
            js1.ExecuteScript(findAndClickOptionScript01);

            IJavaScriptExecutor js2 = (IJavaScriptExecutor)driver;
            js2.ExecuteScript("document.getElementById('select_container_124').click();");
            String findAndClickOptionScript02 =
              "var options = document.querySelectorAll('option, md-option, div[role=\"option\"]');" + // Adjust the selector based on your actual options' elements
              "for(var i = 0; i < options.length; i++) {" +
              "  if(options[i].textContent.trim() === 'General purpose') {" +
              "    options[i].click();" +
              "    break;" +
              "  }" +
              "}";
            js2.ExecuteScript(findAndClickOptionScript02);

            IJavaScriptExecutor js3 = (IJavaScriptExecutor)driver;

            js3.ExecuteScript("document.getElementById('select_value_label_95').click();");

            String findAndClickOptionScript03 =
              "var options = document.querySelectorAll('option, md-option, div[role=\"option\"]');" + // Adjust the selector based on your actual options' elements
              "for(var i = 0; i < options.length; i++) {" +
              "  if(options[i].textContent.trim() === 'N1') {" +
              "    options[i].click();" +
              "    break;" +
              "  }" +
              "}";
            js3.ExecuteScript(findAndClickOptionScript03);

            Thread.Sleep(1000);

            IJavaScriptExecutor js4 = (IJavaScriptExecutor)driver;
            js4.ExecuteScript("document.getElementById('select_value_label_96').click();");
            String findAndClickOptionScript04 =
              "var options = document.querySelectorAll('option, md-option, div[role=\"option\"]');" + // Adjust the selector based on your actual options' elements
              "for(var i = 0; i < options.length; i++) {" +
              "  if(options[i].textContent.trim() === 'n1-standard-8 (vCPUs: 8, RAM: 30GB)') {" +
              "    options[i].click();" +
              "    break;" +
              "  }" +
              "}";
            js4.ExecuteScript(findAndClickOptionScript04);


            string script = @"
            var checkBox = document.evaluate(
                ""//md-checkbox[@ng-model='listingCtrl.computeServer.addGPUs']"",
                document,
                null,
                XPathResult.FIRST_ORDERED_NODE_TYPE,
                null
            ).singleNodeValue;
            if (checkBox) {
                checkBox.click(); // Click the checkbox
            }";

            IJavaScriptExecutor js5 = (IJavaScriptExecutor)driver;
            js5.ExecuteScript(script);

            Thread.Sleep(1000);

            IJavaScriptExecutor js6 = (IJavaScriptExecutor)driver;

            string openDropdownScript = "document.querySelector('md-select[placeholder=\"GPU type\"]').click();";
            js6.ExecuteScript(openDropdownScript);

            string clickOptionScript = @"
            var options = document.querySelectorAll('md-option');
            for(var i = 0; i < options.length; i++) {
                if(options[i].textContent.trim() === 'NVIDIA Tesla T4') {
                    options[i].click();
                    break;
                }
            }";
            js6.ExecuteScript(clickOptionScript);

            Thread.Sleep(1000);

            IJavaScriptExecutor js7 = (IJavaScriptExecutor)driver;

            string openDropdownScriptNumber = "document.querySelector('md-select[placeholder=\"Number of GPUs\"]').click();";
            js7.ExecuteScript(openDropdownScriptNumber);

            string clickOptionScriptNumber = @"
            var options = document.querySelectorAll('md-option');
            for(var i = 0; i < options.length; i++) {
                if(options[i].textContent.trim() === '1') {
                    options[i].click();
                    break;
                }
            }";
            js7.ExecuteScript(clickOptionScriptNumber);

            IJavaScriptExecutor js8 = (IJavaScriptExecutor)driver;
            js8.ExecuteScript("document.getElementById('select_469').click();");
            String findAndClickOptionScript05 =
              "var options = document.querySelectorAll('option, md-option, div[role=\"option\"]');" + 
              "for(var i = 0; i < options.length; i++) {" +
              "  if(options[i].textContent.trim() === '2x375 GB') {" +
              "    options[i].click();" +
              "    break;" +
              "  }" +
              "}";
            js8.ExecuteScript(findAndClickOptionScript05);

            Thread.Sleep(1000);

            IJavaScriptExecutor js10 = (IJavaScriptExecutor)driver;
            js10.ExecuteScript("document.getElementById('select_469').click();");
            String findAndClickOptionScript07 =
              "var options = document.querySelectorAll('option, md-option, div[role=\"option\"]');" + 
              "for(var i = 0; i < options.length; i++) {" +
              "  if(options[i].textContent.trim() === 'Frankfurt (europe-west3)') {" +
              "    options[i].click();" +
              "    break;" +
              "  }" +
              "}";
            js10.ExecuteScript(findAndClickOptionScript07);

            Thread.Sleep(1000);

            IJavaScriptExecutor js11 = (IJavaScriptExecutor)driver;
            js11.ExecuteScript("document.getElementById('select_value_label_99').click();");
            String findAndClickOptionScript08 =
              "var options = document.querySelectorAll('option, md-option, div[role=\"option\"]');" + 
              "for(var i = 0; i < options.length; i++) {" +
              "  if(options[i].textContent.trim() === '1 Year') {" +
              "    options[i].click();" +
              "    break;" +
              "  }" +
              "}";
            js11.ExecuteScript(findAndClickOptionScript08);

            Thread.Sleep(2000);

        }

        public void ClickAddToEstimate()
        {

            IWebElement addButton = driver.FindElement(By.CssSelector(".md-raised.md-primary.cpc-button"));

            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", addButton);
        }

        public bool IsPriceCalculated()
        {
            Thread.Sleep(2000);
            try
            {
                IWebElement costElement = driver.FindElement(By.XPath("//*[@id=\"resultBlock\"]/md-card/md-card-content/div/div/div/div[1]"));

                string costText = costElement.Text;

                string pattern = @"Total Estimated Cost: USD ([\d,]+(\.\d{1,2})?) per 1 month";
                Match match = Regex.Match(costText, pattern);

                if (match.Success)
                {
                    string numericValue = match.Groups[1].Value;

                    decimal costValue = decimal.Parse(numericValue, CultureInfo.GetCultureInfo("en-US"));

                    Console.WriteLine($"Extracted cost: {costValue}");
                    return true;
                }
                else
                {
                    Console.WriteLine($"1.{match}");
                    return false;
                }
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Cost element not found.");
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
                return false;
            }
        }

        public void EmailEstimate()
        {
            IWebElement button = driver.FindElement(By.Id("Email Estimate"));
            button.Click();

            string email = EmailService.FetchTemporaryEmailAndSwitchBack();

            System.Threading.Thread.Sleep(1000);

            EnterEmail(email);
        }

        public void EnterEmail(string email)
        {
            IWebElement emailField = driver.FindElement(By.CssSelector("input[type='email'].md-input"));
            emailField.Clear();
            emailField.SendKeys(email);
        }

        public void SendEmail()
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(1));
                IWebElement buttonElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.CssSelector("#dialogContent_539 > form > md-dialog-actions > button.md-raised.md-primary.cpc-button.md-button.md-ink-ripple")));

                buttonElement.Click();

            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
            }

        }

        public string GetTotalEstimatedCost()
        {
            if (IsPriceCalculated())
            {
                return $"${IsPriceCalculated}";
            }
            else
            {
                return "Price calculation was not successful.";
            }
        }
    }
}
