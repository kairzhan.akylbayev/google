﻿using NUnit.Framework;
using NUnit.Framework.Legacy;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Google
{
    [TestFixture]
    public class Tests
    {
        private IWebDriver driver;
        private GoogleCloudHomePage homePage;
        private GoogleCloudPricingCalculatorPage calculatorPage;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            homePage = new GoogleCloudHomePage(driver);
            calculatorPage = new GoogleCloudPricingCalculatorPage(driver);
        }

        [Test]
        public void TestGoogleCloudPricingCalculator()
        {
            homePage.OpenHomePage();

            homePage.SearchFor("Google Cloud Platform Pricing Calculator");

            homePage.PerformSearch();

            calculatorPage.ClickSecondLink();

            calculatorPage.SwitchToCalculatorFrame();

            calculatorPage.FillOutForm();

            calculatorPage.ClickAddToEstimate();

            ClassicAssert.IsTrue(calculatorPage.IsPriceCalculated(), "Price is not calculated.");

            calculatorPage.EmailEstimate();

            EmailService.FetchTemporaryEmailAndSwitchBack();

            calculatorPage.SendEmail();

        }


        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}