﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace Google
{
    public class GoogleCloudHomePage
    {
        private readonly IWebDriver driver;
        private const string HomePageUrl = "https://cloud.google.com/";
        private IWebElement searchField;

        public GoogleCloudHomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void OpenHomePage()
        {
            driver.Navigate().GoToUrl(HomePageUrl);
        }

        public void SearchFor(string searchTerm)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            searchField = wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("YSM5S")));

            searchField.Click();

            var inputField = driver.FindElement(By.Id("i5"));

            var waitInput = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            waitInput.Until(ExpectedConditions.ElementToBeClickable(inputField));

            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].value = arguments[1];", inputField, "Google Cloud Platform Pricing Calculator");

            inputField.Submit();
        }

        public void PerformSearch()
        {
            var button = driver.FindElement(By.CssSelector(".google-material-icons.PETVs.PETVs-OWXEXe-UbuQg"));

            button.Click();
        }
    }
}
