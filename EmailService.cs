﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace Google
{
    public static class EmailService
    {
        private static IWebDriver driver = new ChromeDriver();
        private static WebDriverWait wait;

        public static string FetchTemporaryEmailAndSwitchBack()
        {
            string originalWindowHandle = driver.CurrentWindowHandle;
            List<string> existingHandles = driver.WindowHandles.ToList();

            ((IJavaScriptExecutor)driver).ExecuteScript("window.open('https://dropmail.me/en/');");

            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(d => driver.WindowHandles.Count > existingHandles.Count);

            string fetchedEmail = string.Empty; // Initialize the email variable
            List<string> newHandles = driver.WindowHandles.Except(existingHandles).ToList();

            foreach (string handle in newHandles)
            {
                driver.SwitchTo().Window(handle);
                string currentURL = driver.Url;
                if (currentURL.Contains("dropmail.me"))
                {
                    wait.Until(ExpectedConditions.ElementExists(By.CssSelector("#app-banner-above > div:nth-child(4) > div > div > div > span.address")));
                    fetchedEmail = driver.FindElement(By.CssSelector("#app-banner-above > div:nth-child(4) > div > div > div > span.address")).Text;
                    break;
                }
            }

            driver.SwitchTo().Window(originalWindowHandle);
            return fetchedEmail; 
        }
    }
}
